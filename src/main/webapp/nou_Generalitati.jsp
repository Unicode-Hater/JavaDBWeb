<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Adauga Generalitate</title>
</head>
<jsp:useBean id="jb" scope="session" class="db.JavaBean" />
<jsp:setProperty name="jb" property="*" />

<body>
    <%
        int id_planta, id_caracteristici;
        
        String id1, id2, nume, nume_stiintific;
        String reproducere, tip, culoare, sezon;
        String specie, regiune, mediu;
        java.sql.Date data_descoperire;
        
        id1 = request.getParameter("id_planta");
        id2 = request.getParameter("id_caracteristici");
        
        specie = request.getParameter("specie");
        
        regiune = request.getParameter("regiune");
        mediu = request.getParameter("mediu");
        
        if (id1 != null) {
        	System.out.println("Ajungi aaaaaaa?");
        	jb.connect();
            jb.adaugaGeneralitate(
                java.lang.Integer.parseInt(id1),
                java.lang.Integer.parseInt(id2),
                specie, regiune, mediu
            );
            jb.disconnect();
    %>
    <p>Datele au fost adaugate.</p>
    <%
        } else {
            jb.connect();
            ResultSet rs1 = jb.vedeTabela("plante");
            ResultSet rs2 = jb.vedeTabela("caracteristici");
    %>
    <h1> Suntem in tabela generalitati.</h1>
    <form action="nou_Generalitati.jsp" method="post">
        <table>
            <tr>
                <td align="right">Id Planta:</td>
                <td>
                    Selectati planta:
                    <SELECT NAME="id_planta">
                        <%
                            while(rs1.next()) {
                                id_planta = rs1.getInt("idplanta");
                                nume = rs1.getString("nume");
                                nume_stiintific = rs1.getString("nume_stiintific");
                                data_descoperire = rs1.getDate("data_descoperire");
                        %>
                        <OPTION VALUE="<%= id_planta%>"><%= id_planta%>,<%= nume%>,<%= nume_stiintific%>,<%= data_descoperire%></OPTION>
                        <%
                            }
                        %>
                    </SELECT>
                </td>
            </tr>
            <tr>
                <td align="right">Id Caracteristici:</td>
                <td>
                    Selectati Caracteristcile:
                    <SELECT NAME="id_caracteristici">
                        <!-- OPTION selected="yes" VALUE="iris1">Iris 1</OPTION -->
                        <%
                            while(rs2.next()) {
                                id_caracteristici = rs2.getInt("idcaracteristici");
                                reproducere = rs2.getString("reproducere");
                                tip = rs2.getString("tip");
                                culoare = rs2.getString("culoare");
                                sezon = rs2.getString("sezon");
                        %>
                        <OPTION VALUE="<%= id_caracteristici%>"><%= id_caracteristici%>,<%= reproducere%>,<%= tip%>,<%= culoare%>,<%= sezon%></OPTION>
                        <%
                            }
                        %>
                    </SELECT>
                </td>
            </tr>
            <tr>
                <td align="right">Specie:</td>
                <td> <input type="text" name="specie" size="30" /></td>
            </tr>
            <tr>
                <td align="right">Regiune:</td>
                <td> <input type="text" name="regiune" size="30" /></td>
            </tr>
            <tr>
                <td align="right">Mediu:</td>
                <td> <input type="text" name="mediu" size="30" /></td>
            </tr>
        </table>
        <input type="submit" value="Adauga Generalitate" />
    </form>
    <%
        }
    %>
    <br />
    <a href="index.html"><b>Home</b></a>
    <br />
</body>

</html>