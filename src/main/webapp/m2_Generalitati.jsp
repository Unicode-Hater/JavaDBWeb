<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Tabela Generalitati</title>
    <link href="table.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<jsp:useBean id="jb" scope="session" class="db.JavaBean" />
<jsp:setProperty name="jb" property="*" />

<body>
    <h1 align="center"> Tabela Generalitati:</h1>
    <br />
    <p align="center"><a href="nou_Generalitati.jsp"><b>Adauga o noua generalitate.</b></a>
        <a href="index.html"><b>Home</b></a></p>
    <%
        jb.connect();
        int aux = java.lang.Integer.parseInt(request.getParameter("idgeneralitati"));
        String id_planta = request.getParameter("idplanta");
        String id_caracteristici = request.getParameter("idcaracteristici");
        String specie = request.getParameter("specie");
        String regiune = request.getParameter("regiune");
        String mediu = request.getParameter("mediu");
        String[] valori = {id_planta, id_caracteristici, specie, regiune, mediu};
        String[] campuri = {"idplanta", "idcaracteristici", "specie", "regiune", "mediu"};
        jb.modificaTabela("generalitati", "idgeneralitati", aux, campuri, valori);
        jb.disconnect();
    %>
    <h1 align="center">Modificarile au fost efectuate !</h1>
    <p align="center">
        <a href="index.html"><b>Home</b></a>
        <br />
</body>

</html>