<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Tabela Consultatie</title>
</head>
<jsp:useBean id="jb" scope="session" class="db.JavaBean" />
<jsp:setProperty name="jb" property="*" />

<body>
    <h1 align="center"> Tabela Generalitati:</h1>
    <br />
    <p align="center"><a href="nou_Generalitati.jsp"><b>Adauga o noua Generalitate.</b></a>
        <a href="index.html"><b>Home</b></a></p>
    <form action="m1_Generalitati.jsp" method="post">
        <table border="1" align="center">
            <tr>
                <td><b>Mark:</b></td>
                <td><b>Id Generalitati:</b></td>
                <td><b>Id Planta:</b></td>
                <td><b>Nume Planta:</b></td>
                <td><b>Nume Stiintific Planta:</b></td>
                <td><b>Data Descoperire Planta:</b></td>
                <td><b>Id Caracteristici:</b></td>
                <td><b>Tip Reproducere:</b></td>
                <td><b>Tip Planta:</b></td>
                <td><b>Culoare:</b></td>
                <td><b>Sezon:</b></td>
                <td><b>Specie:</b></td>
                <td><b>Regiune:</b></td>
                <td><b>Mediu:</b></td>
            </tr>
            <%
                jb.connect();
                ResultSet rs = jb.vedeGeneralitati();
                long id_generalitati;
                
                while (rs.next()) {
                    id_generalitati = rs.getInt("idgeneralitati");
            %>
            <tr>
                <td><input type="checkbox" name="primarykey" value="<%= id_generalitati%>" /></td>
                <td><%= id_generalitati%></td>
                <td><%= rs.getInt("idplanta")%></td>
                <td><%= rs.getString("nume")%></td>
                <td><%= rs.getString("nume_stiintific")%></td>
                <td><%= rs.getDate("data_descoperire")%></td>
                <td><%= rs.getInt("idcaracteristici")%></td>
                <td><%= rs.getString("reproducere")%></td>
                <td><%= rs.getString("tip")%></td>
                <td><%= rs.getString("culoare")%></td>
                <td><%= rs.getString("sezon")%></td>
                <td><%= rs.getString("specie")%></td>
                <td><%= rs.getString("regiune")%></td>
                <td><%= rs.getString("mediu")%></td>
                <%
                }
            %>
            </tr>
        </table><br />
        <p align="center">
            <input type="submit" value="Modifica linia">
        </p>
    </form>
    <%
        jb.disconnect();
    %>
    <br />
    <p align="center">
        <a href="index.html"><b>Home</b></a>
        <br />
    </p>
</body>

</html>