<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Tabela Generalitati</title>
    <link href="table.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<jsp:useBean id="jb" scope="session" class="db.JavaBean" />
<jsp:setProperty name="jb" property="*" />

<body>
    <h1 align="center">Tabela Generalitati:</h1>
    <br />
    <p align="center"><a href="nou_Generalitati.jsp"><b>Adauga o noua generalitate.</b></a>
        <a href="index.html"><b>Home</b></a></p>
    <%
        jb.connect();
        
        int id_planta, id_caracteristici, id1, id2;
        
        String nume, nume_stiintific;
        String reproducere, tip, culoare, sezon;
        String specie, regiune, mediu;
        java.sql.Date data_descoperire;

        int aux = java.lang.Integer.parseInt(request.getParameter("primarykey"));
        
        ResultSet rs = jb.intoarceGeneralitatiId(aux);
        
        rs.first();
        
        id1 = rs.getInt("idplanta");
        id2 = rs.getInt("idcaracteristici");
        
        nume = rs.getString("nume");
        nume_stiintific = rs.getString("nume_stiintific");
        data_descoperire = rs.getDate("data_descoperire");
        reproducere = rs.getString("reproducere");
        tip = rs.getString("tip");
        culoare = rs.getString("culoare");
        specie = rs.getString("specie");
        regiune = rs.getString("regiune");
        mediu = rs.getString("mediu");
        
        ResultSet rs1 = jb.vedeTabela("plante");
        ResultSet rs2 = jb.vedeTabela("caracteristici");
    %>
    <form action="m2_Generalitati.jsp" method="post">
        <table align="center">
            <tr>
                <td align="right">Id Generalitate:</td>
                <td> <input type="text" name="idgeneralitati" size="30" value="<%= aux%>" readonly /></td>
            </tr>
            <tr>
                <td align="right">Id Planta:</td>
                <td>
                    <SELECT NAME="idplanta">
                        <%
                            while (rs1.next()) {
                                id_planta = rs1.getInt("idplanta");
                                nume = rs1.getString("nume");
                                nume_stiintific = rs1.getString("nume_stiintific");
                                data_descoperire = rs1.getDate("data_descoperire");
                                
                                if (id_planta != id1) {
                        %>
                        <OPTION VALUE="<%= id_planta%>"><%= id_planta%>,<%= nume%>,<%= nume_stiintific%>,<%= data_descoperire%></OPTION>
                        <%
                                } else {
                        %>
                        <OPTION selected="yes" VALUE="<%= id_planta%>"><%= id_planta%>,<%= nume%>,<%= nume_stiintific%>,<%= data_descoperire%></OPTION>
                        <%
                                }
                            }
                        %>
                    </SELECT>
                </td>
            </tr>
            <tr>
                <td align="right">Id Caracteristici:</td>
                <td>
                    <SELECT NAME="idcaracteristici">
                        <%
                            while (rs2.next()) {
                                id_caracteristici = rs2.getInt("idcaracteristici");
                                reproducere = rs2.getString("reproducere");
                                tip = rs2.getString("tip");
                                culoare = rs2.getString("culoare");
                                sezon = rs2.getString("sezon");
                                
                                if (id_caracteristici != id2) {
                        %>
                        <OPTION VALUE="<%=id_caracteristici%>"><%=id_caracteristici%>,<%=reproducere%>,<%=tip%>,<%= culoare%>,<%= sezon%></OPTION>
                        <%
                                } else {
                        %>
                        <OPTION selected="yes" VALUE="<%= id_caracteristici%>"><%= id_caracteristici%>,<%= reproducere%>,<%= tip%>,<%= culoare%>,<%= sezon%></OPTION>
                        <%
                                }
                            }
                        %>
                    </SELECT>
                </td>
            </tr>
            <tr>
                <td align="right">Specie:</td>
                <td> <input type="text" name="specie" size="30" value="<%= specie%>" /></td>
            </tr>
            <tr>
                <td align="right">Regiune:</td>
                <td> <input type="text" name="regiune" size="30" value="<%= regiune%>" /></td>
            </tr>
            <tr>
                <td align="right">Mediu:</td>
                <td> <input type="text" name="mediu" size="30" value="<%= mediu%>" /></td>
            </tr>
        </table>
        <p align="center">
            <input type="submit" value="Modifica linia">
        </p>
    </form>
    <p align="center">
<a href=" index.html"><b>Home</b></a>
        <br />
</body>
<%
    rs.close();
    rs1.close();
    rs2.close();
    jb.disconnect();
%>

</html>