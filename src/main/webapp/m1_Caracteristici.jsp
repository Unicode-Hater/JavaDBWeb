<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Tabela Caracteristici</title>
    <link href="table.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<jsp:useBean id="jb" scope="session" class="db.JavaBean" />
<jsp:setProperty name="jb" property="*" />

<body>
    <h1 align="center">Tabela Caracteristici:</h1>
    <br />
    <p align="center"><a href="nou_Caracteristici.jsp"><b>Adauga Caracteristici.</b></a>
        <a href="index.html"><b>Home</b></a></p>
    <%
		jb.connect();
		int aux = java.lang.Integer.parseInt(request.getParameter("primarykey"));
        
		ResultSet rs = jb.intoarceLinieDupaId("caracteristici", "idcaracteristici", aux);
        rs.first();
        
        String reproducere = rs.getString("reproducere");
        String tip = rs.getString("tip");
        String culoare = rs.getString("culoare");
        String sezon = rs.getString("sezon");
        
        rs.close();
        jb.disconnect();
    %>
    <form action="m2_Caracteristici.jsp" method="post">
        <table align="center">
            <tr>
                <td align="right">Id Caracteristici:</td>
                <td> <input type="text" name="idcaracteristici" size="30" value="<%= aux%>" readonly /></td>
            </tr>
            <tr>
                <td align="right">Tip Reproducere:</td>
                <td> <input type="text" name="reproducere" size="30" value="<%= reproducere%>" /></td>
            </tr>
            <tr>
                <td align="right">Tip Planta:</td>
                <td> <input type="text" name="tip" size="30" value="<%= tip%>" /></td>
            </tr>
            <tr>
                <td align="right">Culoare:</td>
                <td> <input type="text" name="culoare" size="30" value="<%= culoare%>" /></td>
            </tr>
            <tr>
                <td align="right">Sezon:</td>
                <td> <input type="text" name="sezon" size="30" value="<%= sezon%>" /></td>
            </tr>
        </table>
        <p align="center">
            <input type="submit" value="Modifica linia">
        </p>
    </form>
    <p align="center">
        <a href="index.html"><b>Home</b></a>
        <br />
</body>

</html>