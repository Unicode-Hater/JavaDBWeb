<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Tabela Caracteristici</title>
</head>
<jsp:useBean id="jb" scope="session" class="db.JavaBean" />
<jsp:setProperty name="jb" property="*" />

<body>
    <h1 align="center"> Tabela Caracteristici:</h1>
    <br />
    <p align="center"><a href="nou_Caracteristici.jsp"><b>Adauga o noua caracteristica.</b></a>
        <a href="index.html"><b>Home</b></a></p>
    <form action="sterge_Caracteristici.jsp" method="post">
        <table border="1" align="center">
            <tr>
            	<td><b>Mark:</b></td>
                <td><b>Id Caracteristica:</b></td>
                <td><b>Reproducere:</b></td>
                <td><b>Tip:</b></td>
                <td><b>Culoare:</b></td>
                <td><b>Sezon:</b></td>
            </tr>
            <%
    			jb.connect();
    			ResultSet rs = jb.vedeTabela("caracteristici");
    			long id_caracteristica;
    			while (rs.next()) {
    				id_caracteristica = rs.getInt("idcaracteristici");
    		%>
            <tr>
                <td><input type="checkbox" name="primarykey" value="<%= id_caracteristica%>" /></td>
                <td><%= id_caracteristica%></td>
                <td><%= rs.getString("reproducere")%></td>
                <td><%= rs.getString("tip")%></td>
                <td><%= rs.getString("culoare")%></td>
                <td><%= rs.getString("sezon")%></td>
            <%
    			}
    		%>
            </tr>
        </table><br />
        <p align="center">
            <input type="submit" value="Sterge liniile marcate">
        </p>
    </form>
    <%
    	rs.close();
    	jb.disconnect();
    %>
    <br />
    <p align="center">
        <a href="index.html"><b>Home</b></a>
        <br />
    </p>
</body>

</html>