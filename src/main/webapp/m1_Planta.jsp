<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Tabela Plante</title>
    <link href="table.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<jsp:useBean id="jb" scope="session" class="db.JavaBean" />
<jsp:setProperty name="jb" property="*" />

<body>
    <h1 align="center">Tabela Plante:</h1>
    <br />
    <p align="center"><a href="nou_Planta.jsp"><b>Adauga o noua planta.</b></a>
        <a href="index.html"><b>Home</b></a></p>
    <%
    	jb.connect();
    	int aux = java.lang.Integer.parseInt(request.getParameter("primarykey"));
    	ResultSet rs = jb.intoarceLinieDupaId("plante", "idplanta", aux);
	    rs.first();
	    String Nume = rs.getString("nume");
		String NumeSttintific = rs.getString("nume_stiintific");
		String DataAux = rs.getString("data_descoperire");
		java.sql.Date Adresa = java.sql.Date.valueOf(DataAux);
		rs.close();
		jb.disconnect();
	%>
    <form action="m2_Planta.jsp" method="post">
        <table align="center">
            <tr>
                <td align="right">Id Planta:</td>
                <td> <input type="text" name="idplanta" size="30" value="<%= aux%>" readonly /></td>
            </tr>
            <tr>
                <td align="right">Nume Planta:</td>
                <td> <input type="text" name="nume" size="30" value="<%= Nume%>" /></td>
            </tr>
            <tr>
                <td align="right">Nume Stiintific:</td>
                <td> <input type="text" name="nume_stiintific" size="30" value="<%= NumeSttintific%>" /></td>
            </tr>
            <tr>
                <td align="right">Data Descoperire:</td>
                <td> <input type="text" name="data_descoperire" size="30" value="<%= DataAux%>" /></td>
            </tr>
        </table>
        <p align="center">
            <input type="submit" value="Modifica linia">
        </p>
    </form>
    <p align="center">
<a href=" index.html"><b>Home</b></a>
        <br />
</body>

</html>